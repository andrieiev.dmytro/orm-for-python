from orm.model import Model
from orm.field import IntegerField, TextField, RealField


class User(Model):
    id = IntegerField(primary_key=True)
    name = TextField(default='default user name')

    class Meta:
        table_name = 'User'


class Man(User):
    sex = TextField()

    class Meta:
        table_name = 'Man'


class Test(Model):
    id = IntegerField(primary_key=True)
    name = TextField(default='test')
    nickname = TextField()
    password = TextField(default='123')

    class Meta:
        table_name = 'Test'


class Test2(Model):
    id = IntegerField(primary_key=True)
    name = TextField()
    surname = TextField()
    address = TextField()
    phone = IntegerField()
    email = TextField()
    password = TextField()

    class Meta:
        table_name = 'Test2'


class Test3(Model):
    id = IntegerField(primary_key=True)
    name = TextField()

    class Meta:
        table_name = 'Test3'


class Test4(Model):
    id = IntegerField(primary_key=True)
    name = TextField()
    foreign_key = IntegerField(foreign_key='Test3')

    class Meta:
        table_name = 'Test4'


class Test5(Model):
    id = IntegerField(primary_key=True)
    name = TextField()
    real_field = RealField()

    class Meta:
        table_name = 'Test5'


class Test6(Model):
    id = IntegerField(primary_key=True)
    foreign_key = IntegerField(foreign_key='Test5')

    class Meta:
        table_name = 'Test6'
