from orm.queryset import QuerySet


class Manager:
    '''
    Manager which manages queries
    Example: Test.objects.create(conn, id=1, name='test')
    '''

    def __init__(self):
        self.model_cls = None

    def __get__(self, instance, owner):
        self.model_cls = owner

        return self

    def create(self, conn, *args, **kwargs):
        return QuerySet(conn, self.model_cls).create(*args, **kwargs)

    def get(self, conn, pk):
        return QuerySet(conn, self.model_cls).get(pk)

    def filter(self, conn, **kwargs):
        return QuerySet(conn, self.model_cls).filter(**kwargs)

    def all(self, conn):
        return QuerySet(conn, self.model_cls)

    def get_all_rows(self, conn):
        return QuerySet(conn, self.model_cls).get_all_rows()

    def get_certain_number_of_rows(self, conn, num):
        return QuerySet(conn, self.model_cls).get_certain_number_of_rows(num)
