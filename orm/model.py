from numbers import Number

from .exceptions import ORMError
from .field import Field
from .manager import Manager


class ModelMeta(type):
    def __new__(mcs, name, bases, namespace):
        if name == 'Model':
            return super().__new__(mcs, name, bases, namespace)

        fields = {}
        pk_field_name = None
        for base in bases:
            if base is not Model and issubclass(base, Model):
                namespace['_table_name'] = base.Meta.table_name
                pk_field_name = base.pk_field_name
                for k, v in base._fields.items():
                    fields[k] = v

        meta = namespace.get('Meta')
        if meta:
            if hasattr(meta, 'table_name'):
                namespace['_table_name'] = meta.table_name
            elif '_table_name' not in namespace:
                raise ValueError('table_name is empty')
        elif '_table_name' not in namespace:
            raise ValueError('meta is none')

        for k, v in namespace.items():
            if isinstance(v, Field):
                fields[k] = v
                if v.primary_key:
                    if pk_field_name is None:
                        pk_field_name = k
                    else:
                        raise RuntimeError('more than one pk')
        if pk_field_name is None:
            raise ORMError('primary key required')

        namespace['pk_field_name'] = pk_field_name
        namespace['_fields'] = fields

        return super().__new__(mcs, name, bases, namespace)


class Model(metaclass=ModelMeta):
    '''
    Class model which manages methods for save new object, inner join,
    delete object, creating new table.
    '''

    # making table name configurable
    class Meta:
        table_name = ''

    objects: Manager = Manager()

    def __init__(self, *_, **kwargs):
        for field_name, field in self._fields.items():
            value = field.validate(kwargs.get(field_name))
            setattr(self, field_name, value)

        if not hasattr(self, 'pk_field_name'):
            raise ORMError('primary key required')

    def save(self, conn):
        field_names = self.get_field_names()
        field_values = tuple(getattr(self, field_name) for field_name in field_names)
        field_names_string = ', '.join(field_names)

        exclude_fields_str = "{0} = EXCLUDED.{0}"
        exclude_fields_query = ", ".join([exclude_fields_str.format(f) for f in field_names[1:]])

        query = f'''INSERT INTO "{self.Meta.table_name}" ({field_names_string}) VALUES {field_values}
                    ON CONFLICT ({field_names[0]}) DO UPDATE SET {exclude_fields_query};'''

        cursor = conn.cursor()

        cursor.execute(query, field_values)

    def inner_join(self, conn, related_model):
        new_field_names = related_model.get_field_names()

        query = f'''SELECT * FROM "{self.Meta.table_name}" INNER JOIN "{related_model.Meta.table_name}"
                    ON "{self.Meta.table_name}".{self.pk_field_name} = "{related_model.Meta.table_name}".{new_field_names[-1]};'''

        cursor = conn.cursor()
        cursor.execute(query)

        rows = cursor.fetchall()

        return rows

    def delete(self, conn):
        query = f'DELETE FROM "{self.Meta.table_name}" WHERE {self.pk_field_name}=%s'
        cursor = conn.cursor()
        cursor.execute(query, (getattr(self, self.pk_field_name),))

    @classmethod
    def create_table(cls, conn, related_model=None):
        fields_string = ', '.join(cls.get_fields_sql_repr())

        if related_model is not None:
            for i in cls.get_field_names():
                if i == 'foreign_key':
                    new_reference_string = f'REFERENCES "{related_model.Meta.table_name}" ({cls.pk_field_name})'
                    query = f'CREATE TABLE IF NOT EXISTS "{cls.Meta.table_name}" ({fields_string} {new_reference_string});'

        else:
            query = f'CREATE TABLE IF NOT EXISTS "{cls.Meta.table_name}" ({fields_string})'

        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()
        return query

    @classmethod
    def get_field_names(cls):
        '''
        Return names of fields
        '''

        return list(cls._fields.keys())

    @classmethod
    def get_fields_sql_repr(cls):
        '''
        Return string which have name of fields with parameters 'NOT NULL, PRIMARY KEY'
        '''

        field_strings = []
        for field_name, field in cls._fields.items():
            primary_key = 'PRIMARY KEY' if field.primary_key else ''
            required = 'NOT NULL' if field.required else ''

            default = ''
            if field.default is not None:
                default_string = field.default if isinstance(field.default, Number) else f"'{field.default}'"
                default = f'DEFAULT {default_string}' if field.default is not None else ''

            field_string = f'{field_name} {field.type} {primary_key} {required} {default}'
            field_strings.append(field_string)

        return field_strings

    def __str__(self):
        return str(vars(self))
