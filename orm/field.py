from .exceptions import FieldError


class Field:
    def __init__(self, f_type, required=True, default=None, primary_key=False, foreign_key=None):
        self.f_type = f_type
        self.required = required
        self.default = default
        self.primary_key = primary_key
        self.foreign_key = foreign_key

    def validate(self, value):
        """
        Check validity of attribute value, when user create new model
        """

        if value is None and not self.required and not self.default:
            raise FieldError('required field is empty')

        if type(value) != self.f_type:
            raise FieldError('wrong value type')

        return self.f_type(value)


class IntegerField(Field):
    type = 'integer'

    def __init__(self, required=True, default=None, primary_key=False, foreign_key=None):
        super().__init__(int, required, default, primary_key, foreign_key)


class TextField(Field):
    type = 'varchar'

    def __init__(self, required=True, default=None, primary_key=False, foreign_key=None):
        super().__init__(str, required, default, primary_key, foreign_key)


class RealField(Field):
    type = 'real'

    def __init__(self, required=True, default=None, primary_key=False, foreign_key=None):
        super().__init__(float, required, default, primary_key, foreign_key)
