import os

import psycopg2
from models import User, Man, Test, Test2, Test3, Test4


def main():
    with psycopg2.connect(database='test', user='postgres', password='postgres', host='localhost',
                          port='5432') as conn:
        conn.autocommit = True

        Test.create_table(conn)
        Test2.create_table(conn)
        Test2.objects.create(conn, id=1, name='Mdma', surname='Pipyao', address='Grechka 1', phone=123,
                             email='123@gmail.com', password='123')
        Test3.create_table(conn)
        test3 = Test3.objects.create(conn, id=78, name='oop')
        test = Test.objects.create(conn, id=78, name='test78', nickname='testio', password='123')
        test2 = Test2.objects.get(conn, pk=1)

        Test.objects.get_all_rows(conn)
        test_get_single_row = Test.objects.get(conn, pk=78)
        print('Test get single row\n', test_get_single_row)

        test_get_all_rows = Test.objects.get_all_rows(conn)
        print('Test get all rows\n', test_get_all_rows)

        test_get_n_rows = Test.objects.get_certain_number_of_rows(conn, 2)
        print('Test get n rows\n', test_get_n_rows)

        test_update_single_row = test.name = 'new test78'
        print('Test update single row\n', test_update_single_row)

        print('Update filtered list of rows')
        for new_test in Test.objects.all(conn).filter(nickname='123'):
            new_test.password = 'password'
            new_test.save(conn)
            print(new_test)

        test_remove_single_row = test.delete(conn)
        print('Test remove single row\n', test_remove_single_row)

        print('Test filter queries')
        for new_test in Test.objects.all(conn).filter(nickname='123'):
            print(new_test)

        print('Test possibility to join related tables and models')
        Test4.create_table(conn, Test3)
        test3 = Test3.objects.create(conn, id=999, name='Oleg')
        test4 = Test4.objects.create(conn, id=5, name='Varya', foreign_key=999)
        print(test3.inner_join(conn, test4))


if __name__ == '__main__':
    main()
