import unittest
import psycopg2
from models import Test5

conn = psycopg2.connect(database='test', user='postgres', password='postgres', host='localhost',
                        port='5432')


class FieldsTestCase(unittest.TestCase):

    def setUp(self) -> None:
        Test5.create_table(conn)
        Test5.objects.create(conn, id=1, name='Test1', real_field=1.1)
        Test5.objects.create(conn, id=2, name='Test2', real_field=1.1)

    def test_integer_field(self):
        test = Test5.objects.get(conn, pk=1)
        self.assertEqual(type(test.id), int)

    def test_varchar_field(self):
        test = Test5.objects.get(conn, pk=1)
        self.assertEqual(type(test.name), str)

    def test_real_field(self):
        test = Test5.objects.get(conn, pk=1)
        self.assertEqual(type(test.real_field), float)
