import unittest
import psycopg2
from models import Test5, Test6

conn = psycopg2.connect(database='test', user='postgres', password='postgres', host='localhost',
                        port='5432')


class QueriesTestCase(unittest.TestCase):

    def setUp(self) -> None:
        Test5.create_table(conn)
        Test5.objects.create(conn, id=1, name='Test1', real_field=1.1)
        Test5.objects.create(conn, id=2, name='Test2', real_field=1.1)

    def test_get_single_row(self):
        test = Test5.objects.get(conn, pk=1)
        self.assertEqual(test.id, 1)
        self.assertEqual(test.name, 'Test1')
        self.assertEqual(test.real_field, 1.1)

    def test_get_all_rows(self):
        test = Test5.objects.get_all_rows(conn)
        assert isinstance(test[0], tuple) and \
               isinstance(test, list)

    def test_get_certain_number_of_rows(self):
        test = Test5.objects.get_certain_number_of_rows(conn, 2)
        self.assertEqual(len(test), 2)

    def test_update_single_row(self):
        test = Test5.objects.get(conn, pk=1)
        test.name = 'new test name'
        test.save(conn)
        self.assertEqual(test.name, 'new test name')

    def test_update_filtered_list_of_rows(self):
        for new_test in Test5.objects.all(conn).filter(real_field=1.1):
            new_test.name = 'new test name'
            new_test.save(conn)
            self.assertEqual(new_test.name, 'new test name')

    def test_remove_single_row(self):
        test = Test5.objects.get(conn, pk=2)
        removed_test_instance = test.delete(conn)
        self.assertEqual(removed_test_instance, None)

    def test_filter_quires(self):
        for test in Test5.objects.all(conn).filter(real_field=1.1):
            self.assertEqual(test.real_field, 1.1)

    def test_creating_table_query(self):
        test = Test5.create_table(conn)
        self.assertEqual(test,
                         'CREATE TABLE IF NOT EXISTS "Test5" (id integer PRIMARY KEY NOT NULL , name varchar  NOT NULL , real_field real  NOT NULL )')

    def test_creating_related_query(self):
        test = Test5.objects.get(conn, pk=1)
        Test6.create_table(conn, test)
        test2 = Test6.objects.create(conn, id=1, foreign_key=1)
        result_of_join = test.inner_join(conn, test2)
        self.assertEqual(result_of_join, [(1, 'Test1', 1.1, 1, 1)])
